"use strict" ;

import EventListener from '../../src/EventListener'

import chai  from 'chai' ;
const { assert } = chai ;

describe( 'system.events.EventListener' , () =>
{
    let listener = new EventListener() ;

    it('listener.handleEvent()', () =>
    {
        assert.isFunction( listener.handleEvent );
    });

    it('listener.toString() === [EventListener]', () =>
    {
        assert.equal( listener.toString() , '[EventListener]' );
    });
});
