
import EventDispatcher from '../../src/EventDispatcher'
import IEventDispatcher from '../../src/IEventDispatcher'

import chai  from 'chai' ;

const { assert } = chai ;

describe( 'system.events.EventDispatcher' , () =>
{
    let dispatcher = new EventDispatcher() ;

    it('dispatcher instanceOf IEventDispatcher', () =>
    {
        assert.instanceOf( dispatcher , IEventDispatcher );
    });

    it('dispatcher instanceOf EventDispatcher', () =>
    {
        assert.instanceOf( dispatcher , EventDispatcher );
    });
});
