/**
 * A library library who provides a simple W3C DOM2/3 Event Model implementation - version: 1.0.0 - license: MPL 2.0/GPL 2.0+/LGPL 2.1+ - Follow me on Twitter! @ekameleon
 */

(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global.vegas_events = factory());
}(this, (function () { 'use strict';

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function _objectSpread(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};
      var ownKeys = Object.keys(source);

      if (typeof Object.getOwnPropertySymbols === 'function') {
        ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) {
          return Object.getOwnPropertyDescriptor(source, sym).enumerable;
        }));
      }

      ownKeys.forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    }

    return target;
  }

  class Identifiable
  {
      constructor( id = null )
      {
          Object.defineProperties( this ,
          {
              id : { value : id , writable : true }
          });
      }
  }

  class ValueObject extends Identifiable
  {
      constructor( init = null )
      {
          super() ;
          if( init )
          {
              this.setTo( init ) ;
          }
      }
      formatToString( className = null , ...rest )
      {
          if( className === null )
          {
              if( !(this._constructorName) )
              {
                  Object.defineProperties( this , { _constructorName : { value : this.constructor.name } } );
              }
              className = this._constructorName ;
          }
          let ar = [ className ] ;
          let len = rest.length ;
          for( let i = 0; i < len ; ++i )
          {
              if( rest[i] in this )
              {
                  ar.push( rest[i] + ":" + this[rest[i]] ) ;
              }
          }
          return "[" + ar.join(' ') + "]" ;
      }
      setTo( init = null )
      {
          if( init )
          {
              for( let prop in init )
              {
                  if( prop in this )
                  {
                      this[prop] = init[prop];
                  }
              }
          }
          return this ;
      }
      toString()
      {
          return this.formatToString( null ) ;
      }
  }

  class Event extends ValueObject {
    constructor() {
      var _this;
      var type = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      var bubbles = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      var cancelable = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
      _this = super();
      _defineProperty(this, "clone", function () {
        return new Event(_this._type, _this._bubbles, _this._cancelable);
      });
      _defineProperty(this, "withTarget", function (target) {
        var event = _this.target ? _this.clone() : _this;
        event._target = target;
        return event;
      });
      _defineProperty(this, "withCurrentTarget", function (currentTarget) {
        _this._currentTarget = currentTarget;
        return _this;
      });
      Object.defineProperties(this, {
        timestamp: {
          value: new Date().valueOf()
        },
        _bubbles: {
          writable: true,
          value: Boolean(bubbles)
        },
        _cancelable: {
          writable: true,
          value: Boolean(cancelable)
        },
        _currentTarget: {
          writable: true,
          value: null
        },
        _defaultPrevented: {
          writable: true,
          value: false
        },
        _eventPhase: {
          writable: true,
          value: 0
        },
        _propagationStopped: {
          writable: true,
          value: false
        },
        _immediatePropagationStopped: {
          writable: true,
          value: false
        },
        _target: {
          writable: true,
          value: null
        },
        _type: {
          writable: true,
          value: type instanceof String || typeof type === 'string' ? type : null
        }
      });
    }
    get bubbles() {
      return this._bubbles;
    }
    get cancelable() {
      return this._cancelable;
    }
    get currentTarget() {
      return this._currentTarget;
    }
    get eventPhase() {
      return this._eventPhase;
    }
    get target() {
      return this._target;
    }
    get type() {
      return this._type;
    }
    isDefaultPrevented() {
      return Boolean(this._defaultPrevented);
    }
    isImmediatePropagationStopped() {
      return this._immediatePropagationStopped;
    }
    isPropagationStopped() {
      return this._propagationStopped;
    }
    preventDefault() {
      if (this._cancelable) {
        this._defaultPrevented = true;
      }
    }
    stopImmediatePropagation() {
      this._immediatePropagationStopped = true;
    }
    stopPropagation() {
      this._propagationStopped = true;
    }
    toString() {
      return this.formatToString(null, "type", "bubbles", "cancelable");
    }
  }
  _defineProperty(Event, "ACTIVATE", "activate");
  _defineProperty(Event, "ADDED", "added");
  _defineProperty(Event, "ADDED_TO_STAGE", "addedToStage");
  _defineProperty(Event, "CANCEL", "cancel");
  _defineProperty(Event, "CHANGE", "change");
  _defineProperty(Event, "CLEAR", "clear");
  _defineProperty(Event, "CLICK", "click");
  _defineProperty(Event, "CLOSE", "close");
  _defineProperty(Event, "COMPLETE", "complete");
  _defineProperty(Event, "CONNECT", "connect");
  _defineProperty(Event, "COPY", "copy");
  _defineProperty(Event, "CUT", "cut");
  _defineProperty(Event, "DEACTIVATE", "deactivate");
  _defineProperty(Event, "FULLSCREEN", "fullScreen");
  _defineProperty(Event, "INIT", "init");
  _defineProperty(Event, "OPEN", "open");
  _defineProperty(Event, "PASTE", "paste");
  _defineProperty(Event, "REMOVED", "removed");
  _defineProperty(Event, "REMOVED_FROM_STAGE", "removedFromStage");
  _defineProperty(Event, "RENDER", "render");
  _defineProperty(Event, "RESIZE", "resize");
  _defineProperty(Event, "SCROLL", "scroll");
  _defineProperty(Event, "SELECT", "select");
  _defineProperty(Event, "UNLOAD", "unload");

  function EventListener() {}
  EventListener.prototype = Object.create(Object.prototype, {
    constructor: {
      writable: true,
      value: EventListener
    },
    handleEvent: {
      writable: true,
      value: function value() {}
    },
    toString: {
      writable: true,
      value: function value() {
        return '[' + this.constructor.name + ']';
      }
    }
  });

  var EventPhase = Object.defineProperties({}, {
    AT_TARGET: {
      value: 2,
      enumerable: true
    },
    BUBBLING_PHASE: {
      value: 3,
      enumerable: true
    },
    CAPTURING_PHASE: {
      value: 1,
      enumerable: true
    },
    NONE: {
      value: 0,
      enumerable: true
    }
  });

  function IEventDispatcher() {}
  IEventDispatcher.prototype = Object.create(Object.prototype, {
    constructor: {
      writable: true,
      value: IEventDispatcher
    },
    addEventListener: {
      writable: true,
      value: function value(type, listener) {
      }
    },
    dispatchEvent: {
      writable: true,
      value: function value(event) {}
    },
    hasEventListener: {
      writable: true,
      value: function value(type) {}
    },
    removeEventListener: {
      writable: true,
      value: function value(type, listener) {
      }
    },
    willTrigger: {
      writable: true,
      value: function value(type) {}
    }
  });

  function EventDispatcher(target) {
    Object.defineProperties(this, {
      target: {
        writable: true,
        value: target instanceof IEventDispatcher ? target : null
      },
      _captureListeners: {
        value: {}
      },
      _listeners: {
        value: {}
      }
    });
  }
  EventDispatcher.prototype = Object.create(IEventDispatcher.prototype, {
    constructor: {
      writable: true,
      value: EventDispatcher
    },
    addEventListener: {
      writable: true,
      value: function value(type, listener) {
        var useCapture = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
        var priority = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0;
        if (!(type instanceof String || typeof type === 'string')) {
          throw new TypeError(this + " addEventListener failed, the type argument must be a valid String expression.");
        }
        if (!(listener instanceof Function || listener instanceof EventListener)) {
          throw new TypeError(this + " addEventListener failed, the listener must be a valid Function or EventListener reference.");
        }
        var collection = useCapture ? this._captureListeners : this._listeners;
        var entry = {
          type: type,
          listener: listener,
          useCapture: useCapture,
          priority: priority
        };
        if (!(type in collection)) {
          collection[type] = [entry];
        } else {
          collection[type].push(entry);
        }
        collection[type].sort(this.compare);
      }
    },
    dispatchEvent: {
      writable: true,
      value: function value(event) {
        if (!(event instanceof Event)) {
          throw new TypeError(this + " dispatchEvent failed, the event argument must be a valid Event object.");
        }
        event = event.withTarget(this.target || this);
        var ancestors = this.createAncestorChain();
        event._eventPhase = EventPhase.CAPTURING_PHASE;
        EventDispatcher.internalHandleCapture(event, ancestors);
        if (!event.isPropagationStopped()) {
          event._eventPhase = EventPhase.AT_TARGET;
          event.withCurrentTarget(event._target);
          var listeners = this._listeners[event.type];
          if (this._listeners[event.type]) {
            EventDispatcher.processListeners(event, listeners);
          }
        }
        if (event.bubbles && !event.isPropagationStopped()) {
          event._eventPhase = EventPhase.BUBBLING_PHASE;
          EventDispatcher.internalHandleBubble(event, ancestors);
        }
        return !event.isDefaultPrevented();
      }
    },
    hasEventListener: {
      writable: true,
      value: function value(type) {
        return Boolean(this._listeners[type] || this._captureListeners[type]);
      }
    },
    removeEventListener: {
      writable: true,
      value: function value(type, listener) {
        var useCapture = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
        if (!(type instanceof String || typeof type === 'string')) {
          throw new TypeError(this + " removeEventListener failed, the type must be a valid String expression.");
        }
        if (!(listener instanceof Function || listener instanceof EventListener)) {
          throw new TypeError(this + " removeEventListener failed, the listener must be a valid Function or EventListener reference.");
        }
        var collection = useCapture ? this._captureListeners : this._listeners;
        var listeners = collection[type];
        if (listeners && listeners.length > 0) {
          var len = listeners.length;
          for (var i = 0; i < len; ++i) {
            if (listeners[i].listener === listener) {
              if (len === 1) {
                delete collection[type];
              } else {
                listeners.splice(i, 1);
              }
              break;
            }
          }
        }
      }
    },
    toString: {
      writable: true,
      value: function value() {
        var exp = '[' + this.constructor.name;
        if (this.target) {
          exp += ' target:' + this.target;
        }
        return exp + ']';
      }
    },
    willTrigger: {
      writable: true,
      value: function value(type) {
        var parents = this.createAncestorChain();
        if (parents instanceof Array && parents.length > 0) {
          var parent;
          var len = parents.length;
          while (--len > -1) {
            parent = parents[len];
            if (parent instanceof IEventDispatcher && parent.hasEventListener(type)) {
              return true;
            }
          }
        }
        return this.hasEventListener(type);
      }
    },
    createAncestorChain: {
      writable: true,
      value: function value() {
        return null;
      }
    },
    compare: {
      value: function value(entry1, entry2) {
        if (entry1.priority > entry2.priority) {
          return -1;
        } else if (entry1.priority < entry2.priority) {
          return 1;
        } else {
          return 0;
        }
      }
    },
    processCapture: {
      value: function value(event) {
        event.withCurrentTarget(this.target || this);
        var listeners = this._captureListeners[event.type];
        if (listeners) {
          EventDispatcher.processListeners(event, listeners);
        }
      }
    },
    processBubble: {
      value: function value(event) {
        event.withCurrentTarget(this.target || this);
        var listeners = this._listeners[event.type];
        if (listeners) {
          EventDispatcher.processListeners(event, listeners);
        }
      }
    }
  });
  Object.defineProperties(EventDispatcher, {
    processListeners: {
      value: function value(event, listeners) {
        if (listeners instanceof Array && listeners.length > 0) {
          var len = listeners.length;
          var listener;
          for (var i = 0; i < len; ++i) {
            listener = listeners[i].listener;
            var flag = void 0;
            if (listener instanceof EventListener) {
              flag = listener.handleEvent(event);
            } else if (listener instanceof Function) {
              flag = listener(event);
            }
            if (flag === false) {
              event.stopPropagation();
              event.preventDefault();
            }
            if (event.isImmediatePropagationStopped()) {
              break;
            }
          }
        }
      }
    },
    internalHandleCapture: {
      value: function value(event, ancestors) {
        if (!(ancestors instanceof Array) || ancestors.length <= 0) {
          return;
        }
        var dispatcher;
        var len = ancestors.length - 1;
        for (var i = len; i >= 0; i--) {
          dispatcher = ancestors[i];
          dispatcher.processCapture(event);
          if (event.isPropagationStopped()) {
            break;
          }
        }
      }
    },
    internalHandleBubble: {
      value: function value(event, ancestors) {
        if (!ancestors || ancestors.length <= 0) {
          return;
        }
        var dispatcher;
        var len = ancestors.length;
        for (var i = 0; i < len; i++) {
          dispatcher = ancestors[i];
          dispatcher.processBubble(event);
          if (event.isPropagationStopped()) {
            break;
          }
        }
      }
    }
  });

  /**
   * The {@link system.events} package provides a W3C Event Model implementation.
   * @summary The {@link system.events} package provides an W3C Event Model library.
   * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
   * @author Marc Alcaraz <ekameleon@gmail.com>
   * @namespace system.events
   * @memberof system
   * @example <caption>Basic usage with a <code>callback</code> function</caption>
   * var click = function( event )
   * {
   *     console.log( "click: " + event ) ;
   * };
   *
   * var dispatcher = new EventDispatcher() ;
   *
   * dispatcher.addEventListener( Event.CLICK , click ) ;
   *
   * dispatcher.dispatchEvent( new Event( Event.CLICK ) ) ;
   * @example <caption>Use the W3C DOM {@link system.events.EventListener|EventListener} interface</caption>
   * var Click = function( name )
   * {
   *     this.name = name ;
   * }
   *
   * Click.prototype = Object.create( EventListener.prototype ,
   * {
   *     constructor : { value : Click } ,
   *     handleEvent : { value : function( event )
   *     {
   *         console.log( this + ' ' + this.name + ' event:' + event ) ;
   *     }}
   * });
   *
   * var click1 = new Click( '#1') ;
   * var click2 = new Click( '#2') ;
   *
   * var dispatcher = new EventDispatcher() ;
   *
   * dispatcher.addEventListener( Event.CLICK , click1 ) ;
   * dispatcher.addEventListener( Event.CLICK , click2 ) ;
   *
   * dispatcher.dispatchEvent( new Event( Event.CLICK ) ) ;
   *
   * dispatcher.removeEventListener( Event.CLICK , click2 ) ;
   * dispatcher.dispatchEvent( new Event( Event.CLICK ) ) ;
   */
  var events = {
    Event: Event,
    EventDispatcher: EventDispatcher,
    EventListener: EventListener,
    EventPhase: EventPhase,
    IEventDispatcher: IEventDispatcher
  };

  var skip = false ;
  function sayHello( name = '' , version = '' , link = '' )
  {
      if( skip )
      {
          return ;
      }
      try
      {
          if ( navigator && navigator.userAgent && navigator.userAgent.toLowerCase().indexOf('chrome') > -1)
          {
              const args = [
                  `\n %c %c %c ${name} ${version} %c %c ${link} %c %c\n\n`,
                  'background: #ff0000; padding:5px 0;',
                  'background: #AA0000; padding:5px 0;',
                  'color: #F7FF3C; background: #000000; padding:5px 0;',
                  'background: #AA0000; padding:5px 0;',
                  'color: #F7FF3C; background: #ff0000; padding:5px 0;',
                  'background: #AA0000; padding:5px 0;',
                  'background: #ff0000; padding:5px 0;',
              ];
              window.console.log.apply( console , args );
          }
          else if (window.console)
          {
              window.console.log(`${name} ${version} - ${link}`);
          }
      }
      catch( error )
      {
      }
  }
  function skipHello()
  {
      skip = true ;
  }

  var metas = Object.defineProperties({}, {
    name: {
      enumerable: true,
      value: 'vegas-js-events'
    },
    description: {
      enumerable: true,
      value: 'A library library who provides a simple W3C DOM2/3 Event Model implementation'
    },
    version: {
      enumerable: true,
      value: '1.0.0'
    },
    license: {
      enumerable: true,
      value: "MPL-2.0 OR GPL-2.0+ OR LGPL-2.1+"
    },
    url: {
      enumerable: true,
      value: 'https://bitbucket.org/ekameleon/vegas-js-events'
    }
  });
  var bundle = _objectSpread({
    metas: metas,
    sayHello: sayHello,
    skipHello: skipHello
  }, events);
  try {
    if (window) {
      window.addEventListener('load', function load() {
        window.removeEventListener("load", load, false);
        sayHello(metas.name, metas.version, metas.url);
      }, false);
    }
  } catch (ignored) {}

  return bundle;

})));
//# sourceMappingURL=vegas.events.js.map
