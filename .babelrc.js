const presets =
[
    [
        "@babel/preset-env" ,
        {
            targets : "> 0.25%, not dead",
            modules : false,
            exclude : ['babel-plugin-transform-classes']
        }
    ]
];

const plugins =
[
    [ "@babel/plugin-transform-runtime" , { helpers:false , useESModules:true } ],
    '@babel/plugin-proposal-class-properties',
    '@babel/plugin-transform-spread'
];

module.exports = { presets, plugins };